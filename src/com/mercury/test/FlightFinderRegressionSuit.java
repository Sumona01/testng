package com.mercury.test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class FlightFinderRegressionSuit {
	
	//ffhb
	WebDriver driver;
	MethodRepository a1= new MethodRepository();
	
	@BeforeMethod
	public void AppLaunch() throws InterruptedException
	{
		a1.browserapplicationlaunch();
	}
		
	/*JAVA Doc
	 * TC_001: Verifying default selection of flight type
	 */
		@Test(priority = 0, enabled=true, description="TC_001: Verifying default selection of flight type")
	public void VerifyDefaultSelectionFlightType()
	{
			try{
				a1.login("dasd","dasd");
				Assert.assertEquals(true, a1.VerifyValidLogin());
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
	}
		
		
		
		/*JAVA Doc
		 * TC_002: Verifying Departing from Value selection
		 */
			@Test(priority = 0, enabled=true, description="TC_002: Verifying Departing from Value selection")
		public void departingFromValueSelection()
		{
				try{
					a1.login("dasd","dasd");
					Assert.assertEquals(true, a1.departingFromValueSelection());
				}
				catch(Exception e)
				{
					System.out.println(e);
				}
		}
				
			
}
