package com.mercury.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class MethodRepository {
	

	
	WebDriver driver;
	public void browserapplicationlaunch() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
	    driver=new ChromeDriver();
	    driver.manage().window().maximize();
	    driver.get("http://newtours.demoaut.com/");
		Thread.sleep(3000);
	}
	public void login(String uname1,String pwd)
	{
		WebElement uname= driver.findElement(By.xpath("//input[@name='userName']"));
		uname.sendKeys(uname1);
		
		WebElement pswd= driver.findElement(By.xpath("//input[@name='password']"));
		pswd.sendKeys(pwd);
		WebElement submit= driver.findElement(By.xpath("//input[@name='login']"));
		submit.click();
	
	}

	public boolean VerifyValidLogin()
	{
		String expTitle = "Find a Flight: Mercury Tours:";
		String actTitle = driver.getTitle();
		if (expTitle.equals(actTitle))
		{
			return true;	
		}
		else {
			return false;
		}
	}
	public boolean VerifyInvalidLogin()
	{
		String expTitle = "Find a Flight: Mercury Tours:";
		String actTitle = driver.getTitle();
		System.out.println(actTitle+""+expTitle );
		if (expTitle!=(actTitle))
		{
			return true;	
		}
		else {
			return false;
		}
	}
	
	public boolean verifyDefaultSelectionRoundTrip()
	{
		WebElement radiobtnOneWay = driver.findElement(By.xpath("//input[@value='oneway']"));
		WebElement radiobtnTrip = driver.findElement(By.xpath("//input[@value='roundtrip']"));
		if (radiobtnOneWay.isSelected()== true && radiobtnTrip.isSelected()==false )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean departingFromValueSelection() throws InterruptedException
	{
		WebElement dropdownDepartingFrom = driver.findElement(By.xpath("//select[@name='fromPort']"));
		Select s1 = new Select(dropdownDepartingFrom);
		s1.selectByVisibleText("London");
		Thread.sleep(5000);
		if(dropdownDepartingFrom.getText().equals("London"))
		{
			return true;
			
		}
		else
		{
			return false;
		}
	}
public void AppClose()
{

driver.quit();

	}
}

