package com.mercury.test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginRegressionSuit {
	WebDriver driver;
	MethodRepository a1= new MethodRepository();
	
	@BeforeMethod
	public void AppLaunch() throws InterruptedException
	{
		a1.browserapplicationlaunch();
	}
/*JAVA Doc
 * TC_001: Verifying Valid Login Functionality
 */
	@Test(priority = 0, enabled=true, description="TC_001: Verifying Valid Login Functionality")
public void verifyValidLogin()
{
		try{
			a1.login("dasd","dasd");
			Assert.assertEquals(true, a1.VerifyValidLogin());
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
}
	
	
	/*JAVA Doc
	 * TC_002: Verifying InValid Login Functionality
	 */
		@Test(priority = 1, enabled=true, description="TC_001: Verifying Valid Login Functionality")
	public void verifyInValidLogin()
	{
			try{
				a1.login("dasd1","dasd2");
				Assert.assertEquals(true, a1.VerifyInvalidLogin());
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
	}
	
	@AfterMethod
	public void appclose()
	{
		a1.AppClose();
	}
}
